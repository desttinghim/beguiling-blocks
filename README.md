# Beguiling Blocks

A tetris clone implemented in Guile scheme with the Chickadee library.

## Cloning

You will need [git lfs](https://git-lfs.github.com/) installed for the assets to
properly download.

## Running

You will need `guile`, `guile-sdl2`, and `guile-chickadee` in order to run this
game. Once you have those things, simply run:

```bash
guile tetris.scm
```
