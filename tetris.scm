;; -*- geiser-scheme-implementation: guile -*-

(use-modules (chickadee)
	     (chickadee math vector)
	     (chickadee math rect)
	     (chickadee render sprite)
	     (chickadee render texture)
	     (chickadee render font)
	     (chickadee render color)
	     (chickadee render shapes)
	     (chickadee scripting agenda)
	     (srfi srfi-43) ;; vectors
       (srfi srfi-28) ;; string formatting
	     )

;;; Commentary:
;;
;; Tetris written in guile scheme using the chickadee library.
;;
;;; Code:


;;
;; Grid2d datatype
;;

(define (make-grid2d w h fill)
  `((w . ,w)
    (h . ,h)
    (vec . ,(make-vector (* w h) fill))))

(define (make-grid2d-vector w h v)
  `((w . ,w)
    (h . ,h)
    (vec . ,v)))

(define (grid2d->vec grid)
  (assq-ref grid 'vec))

(define (grid2d-width grid)
  (assq-ref grid 'w))

(define (grid2d-height grid)
  (assq-ref grid 'h))

(define (grid2d-is-inside? grid x y)
  (let ((x (floor x))
        (y (floor y)))
    (and (and (>= x 0) (<= x (1- (grid2d-width grid))))
         (and (>= y 0) (<= y (1- (grid2d-height grid)))))))

(define (xy->index grid x y)
  (floor (+ x (* y (grid2d-width grid)))))

(define (index->xy grid index)
  `(,(modulo index (grid2d-width grid))
    .
    ,(quotient index (grid2d-width grid))))

(define (get-x v)
  (car v))

(define (get-y v)
  (cdr v))

(define (grid2d-ref grid x y)
  (if (grid2d-is-inside? grid x y)
      (vector-ref (grid2d->vec grid) (xy->index grid x y))
      #f))

(define (grid2d-set! grid val x y)
  (vector-set! (grid2d->vec grid) (xy->index grid x y) val))

(define (grid2d-swap! grid x1 y1 x2 y2)
  (vector-swap! (grid2d->vec grid) (xy->index grid x1 y1) (xy->index grid x2 y2)))

(define (grid2d-for-each f grid)
  (vector-for-each
   (lambda (i x) (apply f (list (index->xy grid i) x)))
   (grid2d->vec grid)))

(define (grid2d-for-row f grid)
  (do ((r 0 (1+ r)))
      ((= r (1- (grid2d-height grid))))
    (apply f (list r (vector-copy
                      (grid2d->vec grid)
                      (xy->index grid 0 r)
                      (+ (xy->index grid 0 r) (grid2d-width grid)))))))

(define (grid2d-for-column f grid)
  (do ((c 0 (1+ c)))
      ((= c (grid2d-width grid)))
    (apply f (list c (vector-unfold
                      (lambda (i x) (values (grid2d-ref grid c i) 0))
                      (1- (grid2d-height grid))
                      0
                      )))))

(define (grid2d-clone grid)
  (make-grid2d-vector
   (grid2d-width grid)
   (grid2d-height grid)
   (vector-copy (grid2d->vec grid))))

(define (grid2d-map f grid)
  (make-grid2d-vector
   (grid2d-width grid)
   (grid2d-height grid)
   (vector-map
    (lambda (i x) (apply f (list (index->xy grid i) x)))
    (grid2d->vec grid))))

(define (grid2d-map! f grid)
  (begin (grid2d-for-each
          (lambda (v x)
            (grid2d-set! grid
                         (apply f (list v x))
                         (get-x v)
                         (get-y v)))
          grid)))

(define (grid2d-clear! grid)
  (grid2d-map!
   (lambda (v x)
     0)
   grid))

(define (grid2d-print src)
  (begin (grid2d-for-each
          (lambda (v val)
            (begin (if (and (eq? (get-x v) 0)
                            (not (eq? (get-y v) 0)))
                       (newline))
                   (display val)))
          src)
         (newline)))

(define (grid2d-transpose src)
  (grid2d-map
   (lambda (v val)
     (grid2d-ref src (get-y v) (get-x v)))
   src))

;; Need to swap x and y dimensions for a proper transpose
;; Following function doesn't work though
  ;;(define (grid2d-transpose src)
  ;; (make-grid2d-vector
  ;;  (grid2d-height src)
  ;;  (grid2d-width src)
  ;;  (vector-map
  ;;   (lambda (i val)
  ;;     (apply (lambda (v val)
  ;;              (grid2d-ref src (get-y v) (get-x v)))
  ;;            (list (index-xy src) val)))
  ;;   (grid2d->vec src))))

(define (grid2d-reverse-rows src)
  (grid2d-map
   (lambda (v val)
     (grid2d-ref src (- (grid2d-width src) (get-x v) 1) (get-y v)))
   src))

(define (grid2d-rotate+90 src)
  (grid2d-reverse-rows
   (grid2d-transpose src)))

(define (grid2d-rotate-90 src)
  (grid2d-transpose
   (grid2d-reverse-rows src)))


;;
;; Piece definitions
;;
(define piece-i
  (make-grid2d-vector
   5 5 #(0 0 0 0 0
           0 0 0 0 0
           0 1 1 1 1
           0 0 0 0 0
           0 0 0 0 0)))

(define piece-j
  (make-grid2d-vector
   5 5 #(0 0 0 0 0
           0 1 0 0 0
           0 1 1 1 0
           0 0 0 0 0
           0 0 0 0 0)))

(define piece-l
  (make-grid2d-vector
   5 5 #(0 0 0 0 0
           0 0 0 1 0
           0 1 1 1 0
           0 0 0 0 0
           0 0 0 0 0)))

(define piece-o
  (make-grid2d-vector
   5 5 #(0 0 0 0 0
           0 0 0 0 0
           0 0 1 1 0
           0 0 1 1 0
           0 0 0 0 0)))

(define piece-s
  (make-grid2d-vector
   5 5 #(0 0 0 0 0
           0 0 0 0 0
           0 0 1 1 0
           0 1 1 0 0
           0 0 0 0 0)))

(define piece-t
  (make-grid2d-vector
   5 5 #(0 0 0 0 0
           0 0 0 0 0
           0 1 1 1 0
           0 0 1 0 0
           0 0 0 0 0)))

(define piece-z
  (make-grid2d-vector
   5 5 #(0 0 0 0 0
           0 0 0 0 0
           0 1 1 0 0
           0 0 1 1 0
           0 0 0 0 0)))

(define pieces
  (list->vector
   `(,piece-i
     ,piece-j
     ,piece-l
     ,piece-o
     ,piece-s
     ,piece-t
     ,piece-z)))

(define pieces-spawn-offset
  #((2 . 17)
    (2 . 17)
    (2 . 17)
    (2 . 16)
    (2 . 16)
    (2 . 16)
    (2 . 16)))

(define (get-piece-offset n)
  (vector-ref pieces-spawn-offset n))

(define (piece-color p n)
  (grid2d-map
   (lambda (v val)
     (if (eq? val 0)
         0
         n))
   p))

(define (get-piece n)
  (piece-color (grid2d-clone (vector-ref pieces n)) (1+ n)))


;;
;; Global variables
;;

(define blocks (make-vector 7 #f))
(define ispaused #f)
(define game-agenda #f)
(define pause-agenda #f)
(define game-over #f)
(define score 0)
(define piece-next #f)
(define piece-next-grid2d #f)

(define key-pause 'p)
(define key-restart 'r)
(define key-down 'down)
(define key-left 'left)
(define key-right 'right)
(define key-drop 'space)
(define key-rot-left 'z)
(define key-rot-right 'x)

(define GRID_EMPTY 0)
(define GRID_BLOCK_BLACK 1)
(define GRID_BLOCK_BLUE 2)
(define GRID_BLOCK_GREEN 3)
(define GRID_BLOCK_RED 4)
(define GRID_BLOCK_WHITE 5)
(define GRID_BLOCK_YELLOW 6)
(define GRID_BLOCK_PURPLE 7)
(define *grid*
  (make-grid2d 10 20 GRID_EMPTY))

(define *grid-cell-size* 16.0)
(define *grid-pixel-offset-x* 16)
(define *grid-pixel-offset-y* 16)
(define *grid-pixel-rect*
  (rect *grid-pixel-offset-x*
	*grid-pixel-offset-y*
	(* *grid-cell-size* (grid2d-width *grid*))
	(* *grid-cell-size* (grid2d-height *grid*))))

(define *piece-grid2d* #f)
(define *piece-x* 0)
(define *piece-y* 0)
(define *piece-exists* #f)

(define (*piece-vec2*)
  (vec2 *piece-x* *piece-y*))


;;
;; Game functions
;;

(define (valid-movement? p x y)
  (call/cc
   (lambda (return)
     (begin (grid2d-for-each
             (lambda (v val)
               (let ((x (+ x (get-x v)))
                     (y (+ y (get-y v))))
                 (when (not (eq? val GRID_EMPTY))
                   (if (not (grid2d-is-inside? *grid* x y)) (return #f))
                   (if (not (eq? (grid2d-ref *grid* x y) GRID_EMPTY))
                       (return #f)))))
             p)
            (return #t)))))

(define (move-piece-left)
  (if (valid-movement? *piece-grid2d* (1- *piece-x*) *piece-y*)
      (set! *piece-x* (1- *piece-x*))))

(define (move-piece-right)
  (if (valid-movement? *piece-grid2d* (1+ *piece-x*) *piece-y*)
      (set! *piece-x* (1+ *piece-x*))))

(define (move-piece-down)
  (if (valid-movement? *piece-grid2d* *piece-x* (1- *piece-y*))
      (set! *piece-y* (1- *piece-y*))))

(define (move-piece-drop)
  (display "not implemented"))

(define (rotate-piece-left)
  (let ((rotated (grid2d-rotate+90 *piece-grid2d*)))
    (if (valid-movement? rotated *piece-x* *piece-y*)
        (set! *piece-grid2d* rotated))))

(define (rotate-piece-right)
  (let ((rotated (grid2d-rotate-90 *piece-grid2d*)))
    (if (valid-movement? rotated *piece-x* *piece-y*)
        (set! *piece-grid2d* rotated))))

(define (integrate-piece)
  (grid2d-for-each
   (lambda (v val)
     (when (not (eq? val GRID_EMPTY))
       (grid2d-set! *grid*
                    val
                    (+ *piece-x* (get-x v))
                    (+ *piece-y* (get-y v)))))
   *piece-grid2d*))

(define cleared-row #f)
(define cleared-row-num 0)
(define (clear-rows)
  (grid2d-for-row
   (lambda (r v)
     (if (vector-fold
             (lambda (i s x) (and s (not (eq? x GRID_EMPTY))))
             #t
             v)
         (begin (if (eq? cleared-row #f)
                    (begin (set! cleared-row r)
                           (set! cleared-row-num 1))
                    (set! cleared-row-num (1+ cleared-row-num)))
                (vector-for-each
                 (lambda (i x) (grid2d-set! *grid* GRID_EMPTY i r))
                 v)
          v)))
   *grid*))

(define (shift-columns)
  (when (not (eq? cleared-row #f))
    (grid2d-for-column
     (lambda (c v)
       (do ((i cleared-row (1+ i)))
           ((= i (- (grid2d-height *grid*) cleared-row-num)))
         (grid2d-swap! *grid* c i c (+ i cleared-row-num))))
     *grid*)
    (set! cleared-row #f)
    (set! cleared-row-num 0)))

(define (piece-random)
  (random (vector-length pieces)))

(define (color-random)
  (random (vector-length blocks)))

(define (spawn-piece)
  (let ((pn piece-next))
    (begin (set! *piece-grid2d* piece-next-grid2d)
           (set! *piece-x* (get-x (get-piece-offset pn)))
           (set! *piece-y* (get-y (get-piece-offset pn)))
           (if (not (valid-movement? *piece-grid2d* *piece-x* *piece-y*))
               (set! game-over #t))
           (set! *piece-exists* #t)
           (set! piece-next (piece-random))
           (set! piece-next-grid2d (get-piece piece-next)))))

(define (update-game)
  (if (not (valid-movement? *piece-grid2d* *piece-x* (1- *piece-y*)))
      (begin (integrate-piece)
             (clear-rows)
             ;; TODO: multiply score by difficulty level
             (set! score (+ score (case cleared-row-num
                                    ((1) 40)
                                    ((2) 100)
                                    ((3) 300)
                                    ((4) 1200)
                                    (else 0))))
             (shift-columns)
             (spawn-piece)))
  (move-piece-down))


;;
;; Drawing Code
;;

;; (define (*xy->pixel* v)
;;   (vec2+ (vec2 *grid-pixel-offset-x* *grid-pixel-offset-y*)
;;          (vec2* v *grid-cell-size*)))

(define (xy->vec2 v)
  (vec2 (get-x v) (get-y v)))

(define (xy->pixel v ox oy cell-size)
  (vec2+ (vec2 ox oy)
         (vec2* (xy->vec2 v) cell-size)))

(define (draw-rect rect)
  (let ((p1 (vec2 (rect-left rect) (rect-bottom rect)))
	(p2 (vec2 (rect-left rect) (rect-top rect)))
	(p3 (vec2 (rect-right rect) (rect-top rect)))
	(p4 (vec2 (rect-right rect) (rect-bottom rect))))
    (draw-line p1 p2)
    (draw-line p2 p3)
    (draw-line p3 p4)
    (draw-line p4 p1)))

(define (draw-grid2d grid ox oy cell-size)
  (grid2d-for-each
   (lambda (v val) (if (not (eq? val GRID_EMPTY))
                       (draw-sprite (vector-ref blocks (1- val))
                                    (xy->pixel v ox oy cell-size))))
   grid))

(define (draw alpha)
  ;; (draw-sprite block (*xy->pixel* (*piece-vec2*)))
  (draw-grid2d *grid*
               *grid-pixel-offset-x*
               *grid-pixel-offset-y*
               *grid-cell-size*)
  (if *piece-exists*
      (draw-grid2d *piece-grid2d*
                   (+ *grid-pixel-offset-x*
                      (* *grid-cell-size* *piece-x*))
                   (+ *grid-pixel-offset-y*
                      (* *grid-cell-size* *piece-y*))
                   *grid-cell-size*))
  (draw-grid2d piece-next-grid2d
               480
               320
               *grid-cell-size*)
  (draw-rect *grid-pixel-rect*)
  (draw-text (format "Score: ~a" score) #v(8 460))
  (if ispaused
      (draw-text (format "Paused. Press ~a to unpause." key-pause) #v(0 0)))
  (if game-over
      (draw-text (format "GAME OVER! Press ~a to restart." key-restart) #v(0 0))))


;;
;; Interface code
;;

(define (init)
  (grid2d-clear! *grid*)

  (set! game-agenda (make-agenda))
  (set! pause-agenda (make-agenda))

  (set! score 0)
  (set! game-over #f)
  (set! ispaused #f)

  (with-agenda game-agenda
               (every 30 (update-game)))
  (set! piece-next (piece-random))
  (set! piece-next-grid2d (get-piece piece-next))
  (spawn-piece))

(define (load)
  (vector-set! blocks 0 (load-image "images/block-black.png"))
  (vector-set! blocks 1 (load-image "images/block-blue.png"))
  (vector-set! blocks 2 (load-image "images/block-green.png"))
  (vector-set! blocks 3 (load-image "images/block-red.png"))
  (vector-set! blocks 4 (load-image "images/block-white.png"))
  (vector-set! blocks 5 (load-image "images/block-yellow.png"))
  (vector-set! blocks 6 (load-image "images/block-purple.png"))

  (init))

(define (update time-advance)
  (if (or ispaused game-over)
      (with-agenda pause-agenda
                   (update-agenda 1))
      (with-agenda game-agenda
                   (update-agenda 1))))

(define (key-press key scancode modifiers repeat?)
  (if (eq? key key-pause) (set! ispaused (not ispaused)))
  (if (eq? key key-restart) (init))
  (if (not (or ispaused game-over))
      (begin
        (if (eq? key key-down) (move-piece-down))
        (if (eq? key key-left) (move-piece-left))
        (if (eq? key key-right) (move-piece-right))
        (if (eq? key key-drop) (move-piece-drop))
        (if (eq? key key-rot-left) (rotate-piece-left))
        (if (eq? key key-rot-right) (rotate-piece-right)))))

(run-game #:window-title "Tetris!"
          #:load   load
          #:update update
          #:key-press key-press
          #:draw   draw)

